<?php

/*

	Template Name: Home

*/

get_template_part('partials/header/template-options');

get_header(); ?>

	<?php get_template_part('partials/home/featured-articles'); ?>

	<section class="main">
			
		<?php get_template_part('partials/sidebar'); ?>

		<?php get_template_part('partials/home/subscribe'); ?>

	</section>

<?php get_footer(); ?>