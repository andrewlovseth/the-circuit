		<footer>
			<div class="wrapper">

				<div class="footer-logo">
					<a href="<?php echo site_url('/'); ?>">
						<img src="<?php $image = get_field('logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="footer-utilities">
					<div class="navigation">
						<div class="header">
							<h5>Navigation</h5>
						</div>

						<div class="links">
							<?php if(have_rows('footer_navigation', 'options')): while(have_rows('footer_navigation', 'options')): the_row(); ?>
		 
							    <div class="link">
							        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
							    </div>

							<?php endwhile; endif; ?>
						</div>
					</div>

					<div class="social">
						<div class="header">
							<h5>Social</h5>
						</div>

						<div class="links">
							<div class="social-link facebook">
								<a href="<?php the_field('facebook', 'options'); ?>" rel="external">
									<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook">
								</a>
							</div>

							<div class="social-link twitter">
								<a href="<?php the_field('twitter', 'options'); ?>" rel="external">
									<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-white.svg" alt="Twitter">
								</a>
							</div>

							<div class="social-link flipboard">
								<a href="<?php the_field('flipboard', 'options'); ?>" rel="external">
									<img src="<?php bloginfo('template_directory') ?>/images/flipboard-icon-white.svg" alt="Flipboard">
								</a>
							</div>
						</div>
					</div>

					<div class="subscribe">
						<div class="header">
							<h5>Subscribe</h5>
						</div>

						<div class="subscribe-form">
							<?php get_template_part('partials/mailchimp-form'); ?>

						</div>
						
					</div>
				</div>

				<div class="copyright">
					<div class="copy">
						<p><?php the_field('copyright', 'options'); ?></p>
					</div>
				</div>

			</div>
		</footer>

	</div> <!-- #page -->

	<?php if(is_single()): ?>
		<?php get_template_part('partials/article/new-user-pop-up'); ?>
	<?php endif; ?>

	<?php get_template_part('partials/header/search-form'); ?>

	<?php get_template_part('partials/subscribe-overlay'); ?>

	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='birthday';fnames[4]='MMERGE4';ftypes[4]='zip';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
	<!--End mc_embed_signup-->
	
	<?php wp_footer(); ?>

	<?php the_field('footer_javascript', 'options'); ?>


	<?php get_template_part('partials/subscribe/viral-loops'); ?>

</body>
</html>