<article class="quick-hit page-content" data-url="<?php the_permalink(); ?>">

	<section class="header">
		<div class="header-wrapper">

			<div class="quick-hit-date">
				<?php if(get_field('tagline')): ?>
					<div class="tagline">
						<h4><?php the_field('tagline'); ?></h4>
					</div>
				<?php endif; ?>
			</div>		

			<div class="title">
				<h1 class="x-large-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			</div>

			<?php get_template_part('partials/article/dek'); ?>

			<?php get_template_part('partials/article/quick-hits/photo'); ?>

			<?php get_template_part('partials/article/byline'); ?>

			<?php get_template_part('partials/article/share'); ?>

		</div>
	</section>
	
	<section class="body p2">
		<div class="show-more-wrapper">
			<?php the_content(); ?>
		</div>

		<div class="show-more-btn">
			<a href="#" class="subscribe-btn btn">Read More</a>
		</div>		
	</section>

</article>
