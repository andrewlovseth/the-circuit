<?php

/*
 * Template Name: Newsletter
 * Template Post Type: post
 */

get_header(); ?>

	<section class="main">

		<section class="subscribe">
			<div class="wrapper">

				<div class="info">
					<?php $homepage = get_option('page_on_front'); ?>

					<div class="headline">
						<h3><?php the_field('subscribe_headline', $homepage ); ?></h1>
					</div>
				
					<div class="cta">
						<a href="#" class="subscribe-btn subscribe-trigger">Subscribe</a>
					</div>
				</div>

				<div class="photo">
					<img src="<?php bloginfo('template_directory') ?>/images/phone-preview-small.png" alt="" />
				</div>

			</div>
		</section>
			
		<?php get_template_part('partials/article/template-newsletter'); ?>

		<section class="newsletter-archive">
			<div class="wrapper">

				<div class="section-header">
					<h2>Recent Daily Kickoff Newsletters</h2>
				</div>

				<div class="recent">					
					<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 8,
							'cat' => 5,
							'order' => 'DESC'
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>


						<article class="archive-post">
							<div class="timestamp">
								<a href="<?php the_permalink(); ?>">
									<span class="day"><?php the_time('D'); ?>, </span><span class="date"><?php the_time('M j'); ?></span>
								</a>
							</div>

							<div class="photo">
								<div class="content">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />											
									</a>
								</div>
							</div>

							<div class="headline">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>
						</article>

					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>



			</div>
		</section>

	</section>
	
<?php get_footer(); ?>