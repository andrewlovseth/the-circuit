<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );
add_theme_support( 'responsive-embeds' );
add_theme_support( 'post-thumbnails' );





// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.2.1
    wp_register_script( 'jquery.3.2.1', 'https://code.jquery.com/jquery-3.2.1.min.js' );
    wp_add_inline_script( 'jquery.3.2.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js' );
    $style_last_updated_at = filemtime($dir . '/style.css' );

    // Add style.css
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' , '', $style_last_updated_at);

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.2.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );




// Add backend styles for Gutenberg
function gutenberg_styles() {
    wp_enqueue_style( 'gutenberg-styles', get_theme_file_uri('gutenberg.css'), false );
}
add_action( 'enqueue_block_editor_assets', 'gutenberg_styles' );

/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );




function wpdocs_excerpt_more( $more ) {
    return '... <a href="'.get_the_permalink().'">Read More</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );










/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);



function add_post_meta_content($content, $post_id) {

    $featured_image = get_post_meta( $post_id, 'featured_image', true );
    $meta = wp_get_attachment_image( $featured_image, 'large' );

    return $meta . $content;
}
add_filter('apple_news_exporter_content_pre', 'add_post_meta_content', 10, 2);



// Add Featured Image to RSS
function featured_image_in_feeds( $content ) {
    global $post;

    if( get_field('featured_image', $post->ID ) ) {
        $featured_image = get_post_meta( $post->ID , 'featured_image', true );
        $content = '<p>' . wp_get_attachment_image( $featured_image, 'large', "", array( "class" => "type:primaryImage" ) ) . '</p>' . $content;
    }
    return $content;
}
add_filter( 'the_excerpt_rss', 'featured_image_in_feeds' );
add_filter( 'the_content_feed', 'featured_image_in_feeds' );



add_action('template_redirect', 'my_custom_disable_author_page');

function my_custom_disable_author_page() {
    global $wp_query;

    if ( is_author() ) {
        $user = get_queried_object();
        $user_url =  get_author_posts_url($user->ID);
        $user_url_modified = str_replace('/author/', '/authors/', $user_url);



        wp_redirect(site_url($user_url_modified), 301); 
        exit; 
    }
}



/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


add_filter('wpseo_opengraph_image', 'bearsmith_custom_og_image');
function bearsmith_custom_og_image($image) {

  if (is_single()) {
    global $post;
    $image = get_field('featured_image', $post->ID);
  }

  return $image['url'];
}
