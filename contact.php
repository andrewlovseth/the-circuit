<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	
	<section class="page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title"><?php the_title(); ?></h1>
			</div>

		</div>
	</section>

	<section class="main">
		<div class="wrapper">
		
			<section class="content copy p2">

				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>

			</section>

		</div>
	</section>




<?php get_footer(); ?>