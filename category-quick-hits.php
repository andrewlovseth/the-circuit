<?php get_header(); ?>

	<section class="main">
			
		<?php get_template_part('partials/sidebar'); ?>

		<section class="article-teasers">
		
			<div class="section-header mobile">
				<h2><a href="<?php echo site_url('/topics/quick-hits/'); ?>">Quick Hits</a></h2>
			</div>

			<?php
				$shortcode = '[ajax_load_more id="quick-hit-pods" loading_style="infinite skype" container_type="div" theme_repeater="pod.php" post_type="post" category="quick-hits" category__not_in="5" scroll_distance="-600"]';
				echo do_shortcode($shortcode);
			?>

		</section>

	</section>
	
<?php get_footer(); ?>