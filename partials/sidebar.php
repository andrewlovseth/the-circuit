<section class="quick-hits sidebar">

	<div class="section-header">
		<h2><a href="<?php echo site_url('/topics/quick-hits/'); ?>">Quick Hits</a></h2>
	</div>		

	<?php
		if(is_category('quick-hits')) {
			$ppp = 20;
		} else {
			$ppp = 10;
		}

		$args = array(
			'post_type' => 'post',
			'posts_per_page' => $ppp,
			'category__in' => array(3, 4) 
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<article class="quick-hit-preview">
			<a href="<?php the_permalink(); ?>">

				<?php if(get_field('featured_image')): ?>
					<div class="photo">
						<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>

				<div class="info">
					<?php if(in_category('quick-hits')): ?>
						<?php if(get_field('tagline')): ?>
							<div class="tagline">
								<h4><?php the_field('tagline'); ?></h4>
							</div>
						<?php endif; ?>

					<?php elseif(in_category('daily-kickoff')): ?>
						<div class="tagline">
							<h4>Daily Kickoff</h4>
						</div>
					<?php endif; ?>

					<div class="title">
						<h3><?php the_title(); ?></h3>
					</div>
				</div>
			</a>
		</article>

	<?php endwhile; endif; wp_reset_postdata(); ?>

</section>