<section id="new-user">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="new-user-close-btn close-btn"></a>
				</div>

				<?php $frontpage_id = get_option('page_on_front'); ?>

				<section class="subscribe">
					<div class="headline">
						<h1 class="x-large-title"><?php the_field('subscribe_headline', $frontpage_id); ?></h1>
					</div>

					<div class="dek">
						<p><?php the_field('subscribe_dek', $frontpage_id); ?></p>
					</div>

					<div class="form">
						<?php get_template_part('partials/mailchimp-form'); ?>
					</div>

					<div class="pullquote">
						<blockquote>
							<p><?php the_field('subscribe_quote', $frontpage_id); ?></p>
						</blockquote>

						<cite>
							<div class="photo">
								<img src="<?php $image = get_field('subscribe_quote_photo', $frontpage_id); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="source">
								<h4><?php the_field('subscribe_quote_name', $frontpage_id); ?></h4>
								<h5><?php the_field('subscribe_quote_details', $frontpage_id); ?></h5>
							</div>
						</cite>
					</div>						
				</section>

			</div>
			
		</div>
	</div>
</section>