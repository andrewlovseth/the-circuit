<div class="quick-hit-date">

	<?php if(is_page_template('quick-hit.php') || is_category('quick-hits')): ?>
		<?php if(get_field('tagline')): ?>
			<div class="tagline">
				<h4><?php the_field('tagline'); ?></h4>
			</div>
		<?php endif; ?>

	<?php elseif(is_page_template('newsletter.php')): ?>
		<div class="tagline">
			<h4>Daily Kickoff</h4>
		</div>
	<?php endif; ?>
</div>		