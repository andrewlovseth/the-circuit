<?php if(get_field('featured_image')): ?>

	<section class="quick-hit-photo">
		<div class="photo-wrapper">
			<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php get_template_part('partials/photo-credit'); ?>
		</div>

		<?php if($image['caption']): ?>
			<div class="caption">
				<p><?php echo $image['caption']; ?></p>
			</div>
		<?php endif; ?>	
	</section>

<?php endif; ?>