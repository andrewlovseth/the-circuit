<?php if(get_field('tagline')): ?>
    <div class="tagline">
        <strong><?php the_field('tagline'); ?></strong>
    </div>
<?php endif; ?>
