<article class="newsletter quick-hit page-content">

	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 8,
			'cat' => 5,
			'order' => 'DESC'
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : ?>

			<div class="recent-dropdown">
				<div class="recent-dropdown-wrapper">
					<div class="recent-dropdown-outline">

						<div class="dropdown-header">
							<span>Recent Issues</span>
						</div>

						<div class="combo-box">
							<select>
								<option value="">- Select -</option>
								<?php while ( $query->have_posts() ) : $query->the_post(); ?>
									<option value="<?php the_permalink(); ?>"><?php the_time('D'); ?>, </span><span class="date"><?php the_time('M j'); ?></option>
								<?php endwhile; ?>	
							</select> 					
						</div>
						
					</div>
				</div>
			</div>

	<?php endif; wp_reset_postdata(); ?>

	<section class="header">
		<div class="header-wrapper">

			<div class="title">
				<h2 class="post-type">Daily Kickoff</h2>
		<!--		<h1 class="large-title"><?php the_title(); ?></h1> -->
			</div>

			<div class="date">
				<span><?php the_time('F j, Y'); ?></span>
			</div>		

			<?php get_template_part('partials/article/share'); ?>

		</div>
	</section>

	<?php get_template_part('partials/article/body'); ?>

</article>