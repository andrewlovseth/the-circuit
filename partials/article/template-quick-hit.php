<article class="quick-hit page-content">

	<section class="header">
		<div class="header-wrapper">

			<?php get_template_part('partials/article/quick-hits/tagline'); ?>
			
			<?php get_template_part('partials/article/title'); ?>

			<?php get_template_part('partials/article/dek'); ?>

			<?php get_template_part('partials/article/quick-hits/photo'); ?>

			<?php get_template_part('partials/article/quick-hits/byline'); ?>

			<?php get_template_part('partials/article/share'); ?>

			<?php get_template_part('partials/article/dateline'); ?>		

		</div>
	</section>

	<?php get_template_part('partials/article/body'); ?>

</article>