<?php if(get_field('dek')): ?>

	<div class="dek">
		<p><?php the_field('dek'); ?></p>
	</div>

<?php endif; ?>