<section class="featured-image">
	<div class="content">
		<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['x-large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<?php get_template_part('partials/photo-credit'); ?>
</section>