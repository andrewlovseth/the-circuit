<?php if(get_field('dateline')): ?>
	<div class="dateline">
		<h5><?php the_field('dateline'); ?> &mdash;</h5>				
	</div>
<?php endif; ?>