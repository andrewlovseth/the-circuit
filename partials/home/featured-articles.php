<section class="featured-articles">
	<?php $posts = get_field('featured_articles'); if( $posts ): ?>
		<?php foreach( $posts as $p ): ?>

			<?php $image = get_field('featured_image', $p->ID); ?>

			<article data-thumb="<?php echo $image['sizes']['medium']; ?>">
				<a href="<?php echo get_permalink($p->ID); ?>">
					<div class="photo">
						<div class="content">
							<img src="<?php echo $image['sizes']['x-large']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>
				
					<div class="info">
						<div class="info-wrapper">
							<div class="headline">
								<h1 class="x-large-title">
									<?php echo get_the_title($p->ID); ?>
								</h1>
							</div>
						
							<div class="dek">
								<p><?php the_field('dek', $p->ID); ?></p>
							</div>
						</div>
					</div>
				</a>
			</article>

		<?php endforeach; ?>
	<?php endif; ?>
</section>