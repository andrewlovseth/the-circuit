<section class="newsletter-sidebar sidebar">

	<div class="section-header">
		<h2><a href="<?php echo site_url('/topics/daily-kickoff/'); ?>">Archive</a></h2>
	</div>		

	<?php
		$args = array(
			'post_type' => 'post',
			'posts_per_page' => 7,
			'category__in' => array(5)
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

		<article class="newsletter-preview">
			<h3><a href="<?php the_permalink(); ?>"><span><?php the_time('D, M j'); ?></span></a></h3>
		</article>

	<?php endwhile; endif; wp_reset_postdata(); ?>

</section>