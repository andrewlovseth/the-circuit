<?php

  $headline = get_the_title();
  $image = get_field('featured_image');
  $date_published = get_the_date('c');
  $date_modified = get_the_modified_date('c');

?>

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "headline": "<?php echo $headline; ?>",
      "image": [
        "<?php echo $image['sizes']['thumbnail']; ?>",
        "<?php echo $image['sizes']['large']; ?>"
       ],
      "datePublished": "<?php echo $date_published; ?>",
      "dateModified": "<?php echo $date_modified; ?>"
    }
</script>