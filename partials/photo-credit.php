<?php $image = get_field('featured_image'); if($image['description']): ?>
	<div class="credit">
		<p><?php echo $image['description']; ?></p>
	</div>
<?php endif; ?>