<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<div class="headline">
				<h1 class="x-large-title"><?php single_cat_title(); ?></h1>
			</div>
			
		</div>
	</section>			

	<section class="main">
		<div class="wrapper">

			<?php if ( have_posts() ): $count = 1; ?>

				<section class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<article class="post-<?php echo $count; ?>">
							<div class="photo">
								<div class="content">
									<a href="<?php the_permalink(); ?>">
										<img src="<?php $image = get_field('featured_image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
									</a>								
								</div>
							</div>

							<div class="info">

								<div class="meta">
									<span class="date"><?php the_time('F j, Y'); ?></span>
								</div>
								
								<div class="headline">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>

								<?php if($count == 1): ?>
									<div class="copy p2">
										<p><?php the_field('dek'); ?></p>
									</div>
								<?php endif; ?>							
								
							</div>


						</article>

					<?php $count++; endwhile; ?>

				</section>

				<?php
					the_posts_pagination(
						array(
							'mid_size'  => 1,
							'prev_text' => __('Prev'),
							'next_text' => __('Next'),
						)
					);
				?>

			<?php endif; ?>

		</div>
	</section>
	
<?php get_footer(); ?>