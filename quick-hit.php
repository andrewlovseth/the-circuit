<?php

/*
 * Template Name: Quick Hit
 * Template Post Type: post
 */

get_header(); ?>

	<section class="main">
			
		<?php get_template_part('partials/sidebar'); ?>

		<?php get_template_part('partials/article/template-quick-hit'); ?>

	</section>
	
<?php get_footer(); ?>