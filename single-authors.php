<?php

$author_id = get_the_ID();
get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<div class="photo">
				<img src="<?php $image = get_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
			
			<div class="headline">
				<h1 class="x-large-title">
					<span class="name"><?php the_title(); ?></span>

					<?php if(get_field('twitter')): ?>
						<a class="twitter" href="<?php the_field('twitter'); ?>" rel="external">
							<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-blue.svg" alt="Twitter" />
						</a>
					<?php endif; ?>						

					<?php if(get_field('email')): ?>
						<a class="email" href="mailto:<?php the_field('email'); ?>">
							<img src="<?php bloginfo('template_directory') ?>/images/email-icon-red.svg" alt="Email" />
						</a>
					<?php endif; ?>		

				</h1>

				<?php if(get_field('title')): ?>
					<h3 class="title"><?php the_field('title'); ?></h3>
				<?php endif; ?>
			</div>

			<div class="copy p2">
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>	

	<section class="results">
		<div class="wrapper">

			<?php
				$author_posts = get_posts(array(
					'post_type' => 'post',
					'posts_per_page' => -1,
					'meta_query' => array(
						array(
							'key' => 'authors',
							'value' => '"' . get_the_ID() . '"',
							'compare' => 'LIKE'
						)
					)
				));


				$author_post_array = array();
				foreach($author_posts as $author_post) {
					array_push($author_post_array, $author_post->ID);
				}

				$author_post_ids = implode(', ', $author_post_array);

				if($author_post_ids != ''): ?>

					<div class="section-header headline">
						<h2>Recent Articles by <?php the_title(); ?></h2>
					</div>

					<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="10" post__in="' . $author_post_ids .'" scroll="false"]'); ?>

				<?php endif ; ?>

		</div>
	</section>
	
<?php get_footer(); ?>