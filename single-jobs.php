<?php get_header(); ?>

	<section class="page-header">
		<div class="wrapper">

			<div class="meta">
				<div class="category">
					<span><?php the_field('category'); ?></span>
				</div>

				<div class="location">
					<span><?php the_field('location'); ?></span>
				</div>
			</div>

			<div class="headline">
				<h1 class="x-large-title"><?php the_title(); ?></h1>
			</div>

		</div>
	</section>

	<section class="body p1">
		<div class="wrapper">
	
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; endif; ?>

			<div class="cta">
				<?php 
				$link = get_field('appy_link');
				if( $link ): 
				    $link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
				    ?>
				    <a class="subscribe-btn clear-charcoal" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php endif; ?>	
			</div>

		</div>
	</section>
	
<?php get_footer(); ?>