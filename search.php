<?php

$search_query = get_search_query();

get_header(); ?>

	<section class="page-header">
		<div class="wrapper">
			
			<div class="headline">
				<h1 class="x-large-title">Search Results: <?php echo $search_query; ?></h1>
			</div>

		</div>
	</section>	

	<section class="results">
		<div class="wrapper">
		
			<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

				<?php get_template_part('partials/archive-teaser'); ?>

		    <?php endwhile; endif; ?>
		
			<?php
				the_posts_pagination(
					array(
						'mid_size'  => 1,
						'prev_text' => __('Prev'),
						'next_text' => __('Next'),
					)
				);
			?>

		</div>
	</section>
	
<?php get_footer(); ?>