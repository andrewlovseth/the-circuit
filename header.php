<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="google-site-verification" content="p_yKTYgn72OnvCEQ5ApWHAh8tuEXW1VI6VUkVMVp_oE" />
	
	<?php wp_head(); ?>

	<?php if(is_single()): $image = get_field('featured_image'); $dek = get_field('dek') ?>
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:image" content="<?php echo $image['sizes']['large']; ?>" />
		<meta name="twitter:description" content="<?php echo $dek; ?>" />
	
		<?php // get_template_part('partials/google-news'); ?>
	<?php endif; ?>
	

</head>

<body <?php body_class(); ?>>

	<?php 
		global $theme;
	?>

	<?php get_template_part('partials/header/nav'); ?>
	
	<div id="page">

		<header<?php if($theme == 'overlay'): ?> class="header-overlay"<?php endif; ?>>
			<div class="wrapper">

				<div class="logo">
					<a class="logo-type" href="<?php echo site_url('/'); ?>">
						<?php if($theme == 'overlay'): ?>
							<img src="<?php $image = get_field('logo_white', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php else: ?>
							<img src="<?php $image = get_field('logo_color', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php endif; ?>
					</a>
				</div>

				<div class="desktop-nav-links">
					<?php if(have_rows('desktop_nav_links', 'options')): while(have_rows('desktop_nav_links', 'options')): the_row(); ?>
 
					    <a href="<?php the_sub_field('link'); ?>"><span><?php the_sub_field('label'); ?></span></a>

					<?php endwhile; endif; ?>
				</div>

				<div class="utilities">

					<div class="social">
						<div class="social-link facebook">
							<a href="<?php the_field('facebook', 'options'); ?>" rel="external">
								<?php if($theme == 'overlay'): ?>
									<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook" />
								<?php else: ?>
									<img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-black.svg" alt="Facebook" />
								<?php endif; ?>
							</a>
						</div>

						<div class="social-link twitter">
							<a href="<?php the_field('twitter', 'options'); ?>" rel="external">
								<?php if($theme == 'overlay'): ?>
									<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-white.svg" alt="Twitter" />
								<?php else: ?>
									<img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-black.svg" alt="Twitter" />
								<?php endif; ?>
							</a>
						</div>					
					</div>

					<div class="subscribe">
						<a href="<?php echo site_url('/#subscribe'); ?>" class="subscribe-trigger subscribe-btn">Subscribe</a>
					</div>

					<div class="search">
						<a href="#" class="search-btn search-trigger">
							<?php if($theme == 'overlay'): ?>
								<img src="<?php bloginfo('template_directory') ?>/images/search-icon.svg" alt="Search" />
							<?php else: ?>
								<img src="<?php bloginfo('template_directory') ?>/images/search-icon-black.svg" alt="Search" />
							<?php endif; ?>
						</a>
					</div>

					<a href="#" id="toggle">
						<div class="patty"></div>
					</a>				
				</div>


			</div>
		</header>