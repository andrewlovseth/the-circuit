<?php

get_template_part('partials/header/template-options');

get_header(); ?>

	<article class="feature page-content">

		<?php get_template_part('partials/article/featured-image'); ?>

		<section class="header">
			<div class="header-wrapper">

				<?php get_template_part('partials/article/title'); ?>

				<?php get_template_part('partials/article/dek'); ?>

				<?php get_template_part('partials/article/byline'); ?>

				<?php get_template_part('partials/article/share'); ?>

				<?php get_template_part('partials/article/dateline'); ?>		

			</div>
		</section>

		<?php get_template_part('partials/article/body'); ?>

		<?php get_template_part('partials/article/footer'); ?>

	</article>
	
<?php get_footer(); ?>